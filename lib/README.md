# Archivos de la librería

* `foo-library.js`. Librería comentada para estudiarse o usarse.
* `foo-library.min.js`. Librería minificada lista para usarse.

## Estructura

La estructura general es la siguiente:

![Estructura general](main_structure.png)

* `#foo-main`. Contiene todo lo relativo al juego, siempre está centrado y lo más grande posible.
* `#foo-head`. Contiene el encabezado donde va el temporizador y el puntaje.
* `#foo-adds`. Contiene el apartado para poner anuncios relativos al juego, está por encima de `#foo-game` y sin mostrarse.
* `#foo-game`. Contiene el juego.

La estructura del encabezado es la siguiente:

![head_structure.png](head_structure.png)

El encabezado está dividido en tres:

* `0`. Por defecto es el bloque para el temporizador (`#foo-timer`)
  que contiene un párrafo para la etiqueta (`#foo-timer-label`) y
  otro para la cantidad `(#foo-timer-total)`.
* `1`. Por defecto está vacío y podría llenarse con algún otro contenido.
* `2`. Por defecto es el bloque para el puntaje (`#foo-score`)
  que contiene un párrafo para la etiqueta (`#foo-score-label`) y
  otro para la cantidad `(#foo-score-total)`.

## Elementos

Para las opciones aplicables desde el JSON, véase el README del directorio `json`.

Existen 7 elementos de `foo`:

1. `foo.data`. `Object` que da acceso a varios datos del juego.
2. `foo.create()`. `Function` que crea un juego tipo Foo.
3. `foo.destroy()`. `Function` que destruye el juego.
4. `foo.disable()`. `Function` que pausa el juego.
5. `foo.enable()`. `Function` que continúa el juego.
6. `foo.save()`. `Function` que guarda el juego.
7. `foo.wipe()`. `Function` que borra datos del juego

Todas las funciones, excepto `foo.wipe()` tienen dos parámetros opcionales:

* `on_init`. `Function || String` que indica una función a ejecutar antes de realizar todas las acciones.
* `on_finish`. `Function || String` que indica una función a ejecutar después de realizar todas las acciones.

### `foo.data`

El objeto `foo.data` contiene los siguientes elementos:

* `type`. `String` que indica el tipo de juego: `story` o `arcade`.
* `active`. `Bool` que indica si el juego está activo.
* `finished`. `Bool` que indica si el juego fue terminado satisfactoriamente.
* `time`. `Integer` que indica el tiempo actual.
* `score`. `Integer` que indica el puntaje actual.
* `past_score`. `Integer` que indica el puntaje pasado.
* `line`. `Object` que indica el elemento DOM de la línea más reciente en juego.
* `line_correct`. `String` que indica la respuesta correcta de la línea más reciente del juego.
* `lines_correct`. `Integer` que indica el total de líneas correctas.
* `lines_incorrect`. `Integer` que indica el total de líneas incorrectas.
* `letter`. `Object` que indica el elemento DOM de la letra más reciente en juego.
* `new_words`. `Array` que indica las palabras nuevas encontradas en el juego actual según el orden de aparición.

### `foo.create()`

La función `foo.create()` tiene varios parámetros adicionales en cualquier modo de juego:

* `type`. `String` que indica el tipo de juego. Por defecto `null`.
* `url`. `String` que indica la ruta al archivo JSON; si hay una partida guardada para el mismo tipo de juego, usará eso en su lugar. Por defecto `null`.
* `lang`. `String` qie indica el lenguaje del juego; solo disponible `es || en`. Por defecto `es`.
* `size`. `Array` de 2 elementos (`x`, `y`) que indica el tamaño relativo del juego; según el tamaño de la pantalla, se ajustará de manera acorde. Por defecto `[7,13]`.
* `margin`. `Array` de 4 elementos (`top`, `right`, `bottom`, `left`) que indica el tamaño relativo de los márgenes del juego. Por defecto `[2,1,1,1]`.
* `border`. `Array` de 2 elementos (`top`, `bottom`) que indica el grosor de los bordes. Por defecto `[0,0]`.
* `score_position`. `Integer` que indica la posición del panel del puntaje; el número es relativo a la estructura del encabezado. Por defecto `2`.
* `timer_position`. `Integer` que indica la posición del panel del temporizador; el número es relativo a la estructura del encabezado. Por defecto `0`.
* `score_label`. `String` que indica el nombre a mostrar para el puntaje. Por defecto `Puntos`.
* `timer_label`. `String` que indica el nombre a mostrar para el temporizador. Por defecto `Tiempo`.
* `border_style`. `String` que indica el [tipo de borde](https://www.w3schools.com/CSSref/pr_border-style.asp). Por defecto `solid`.
* `border_color`. `String` que indica el [color del borde](https://www.w3schools.com/cssref/pr_border-color.asp). Por defecto `black`.
* `game_fontSize`. `String` que indica el [tamaño de fuente](https://www.w3schools.com/cssref/pr_font_font-size.asp) del juego, excepto del puntaje, del temporizador, de las letras y de las oportunidades. Por defecto `1em`.
* `game_fontFamily`. `String` que indica la [familia tipográfica] del juego, excepto del puntaje y temporizador. Por defecto `Impact, Charcoal, sans-serif`.
* `labels_fontSize`. `String` que indica el [tamaño de fuente](https://www.w3schools.com/cssref/pr_font_font-size.asp) de la etiqueta del puntaje y del temporizador. Por defecto `.75em`.
* `labels_fontFamily`. `String` que indica la [familia tipográfica] de la etiqueta del puntaje y del temporizador. Por defecto `Impact, Charcoal, sans-serif`.
* `totals_fontSize`. `String` que indica el [tamaño de fuente](https://www.w3schools.com/cssref/pr_font_font-size.asp) de la cantidad del puntaje y del temporizador. Por defecto `1.5em`.
* `totals_fontFamily`. `String` que indica la [familia tipográfica] de la cantidad del puntaje y del temporizador. Por defecto `Impact, Charcoal, sans-serif`.
* `lt_fontSize`. `String` que indica el [tamaño de fuente](https://www.w3schools.com/cssref/pr_font_font-size.asp) de las letras. Por defecto `1em`.
* `lt_chance_fontSize`. `String` que indica el [tamaño de fuente](https://www.w3schools.com/cssref/pr_font_font-size.asp) de las oportunidades. Por defecto `.5em`.
* `lt_bkcolor_correct`. `String` que indica el [fondo](https://developer.mozilla.org/en-US/docs/Web/CSS/background) de la letra cuando es correcta; se despliega hasta que la línea esté completada. Por defecto `green`.
* `lt_bkcolor_incorrect`. `String` que indica el [fondo](https://developer.mozilla.org/en-US/docs/Web/CSS/background) de la letra cuando es incorrecta; se despliega hasta que la línea esté completada. Por defecto `red`.
* `lt_bkcolor_enable`. `String` que indica el [fondo](https://developer.mozilla.org/en-US/docs/Web/CSS/background) de la letra cuando está habilitada; es el color cuando aún hay oportunidades para cambiar la letra. Por defecto `white`.
* `lt_bkcolor_disable`. `String` que indica el [fondo](https://developer.mozilla.org/en-US/docs/Web/CSS/background) de la letra cuando está inhabilitada; es el color cuando ya no hay oportunidades para cambiar la letra. Por defecto `#ddd`.
* `bar_style`. `String` que indica el estilo de la barra de progreso, que es un [borde](https://www.w3schools.com/css/css_border.asp). Por defecto `1px solid #ccc`.
* `on_win`. `Function || String` que indica la función a ejecutar cuando se gana el juego. Por defecto `''`.
* `on_lose`. `Function || String` que indica la función a ejecutar cuando se pierde el juego. Por defecto `''`.
* `on_error`. `Function || String` que indica la función a ejecutar cuando no se pueden cargar los datos del juego. Por defecto `''`.
* `on_new_time`. `Function || String` que indica la función a ejecutar cuando el temporizador se actualiza.
* `on_new_score`. `Function || String` que indica la función a ejecutar cuando el puntaje se actualiza.
* `on_new_word`. `Function || String` que indica la función a ejecutar cuando se encuentra una nueva palabra.
* `on_new_record`. `Function || String` que indica la función a ejecutar cuando se obtiene un puntaje de los más altos.

Para la modalidad `arcade`, existen estos parámetros adicionales:

* `ar_chances`. `Integer` que indica la cantidad de oportunidades para elegir la letra. Por defecto `3`.
* `ar_min_answers`. `Integer` que indica el mínimo de opciones en cada letra; de manera automática aumenta hasta ser igual a `ar_max_answers_total - 1`. Por defecto `2`.
* `ar_max_answers`. `Integer` que indica el máximo de opciones en cada letra; de manera automática aumenta hasta ser igual a `ar_max_answers_total`. Por defecto `3`.
* `ar_max_answers_total`. `Integer` que indica el máximo posible de opciones en cada letra. Por defecto `6`.
* `ar_bomb_or_bonus`. `Integer` que indica la posibilidad de que aparezca una bomba o un bono; de manera aleatoria se eligirá un número entre el estipulado y con índice 0; si es `0` es una bomba; si es `1` es un bono. Por defecto `20`.
* `ar_random_first_letter`. `Integer` que indica la posibilidad de que aparezca una letra correcta; de manera aleatoria se eligirá un número entre el estipulado y con índice 0; si es `0` es una letra correcta;. Por defecto `3`.
* `ar_length`. `Integer` que indica el máximo de líneas posibles; se manera aleatoria se toman del JSON de `arcade`. Por defecto `10`.
* `ar_on_ten_seconds`. `Function || String` que indica la función a ejecutar cuando se cuentan con 10 segundos. Por defecto `''`.
* `ar_on_line_completed`. `Function || String` que indica la función a ejecutar cuando se completa una línea. Por defecto `''`.
* `ar_on_line_correct`. `Function || String` que indica la función a ejecutar cuando se completa una línea correctamente. Por defecto `''`.
* `ar_on_line_incorrect`. `Function || String` que indica la función a ejecutar cuando se completa una línea incorrectamente. Por defecto `''`.
* `ar_on_first_change`. `Function || String` que indica la función a ejecutar cuando se presiona por primera vez una letra. Por defecto `''`.
* `ar_on_change`. `Function || String` que indica la función a ejecutar cuando se presiona una letra. Por defecto `''`.
* `ar_on_loop`. `Function || String` que indica la función a ejecutar cuando se cumple todo un ciclo de opciones de una letra. Por defecto `''`.
* `ar_on_bomb`. `Function || String` que indica la función a ejecutar cuando se descubre una bomba. Por defecto `''`.
* `ar_on_bonus`. `Function || String` que indica la función a ejecutar cuando se descubre un bono. Por defecto `''`.

Estas opciones no están disponibles en el modo `story` ya que se 
supone que se han de poner a discreción directamente sobre el JSON 
para tener una amplia gama de personalización.    

### `foo.wipe()`

La función `foo.wipe()` solo tiene un parámetro necesario disponible:

* `all || both || story || arcade || data`. `String` que indica qué borrar.
    * `all`. Borra todo lo guardado en ambas modalidades.
    * `both`. Borra la partida guardada y el mapa de bombas y bonos activado en ambas modalidades; no borra los puntajes más altos o la lista de palabras obtenidas.
    * `story`. Borra la partida guardada de modo historia y su mapa de bombas o bonos activados; no borra los puntajes más altos o la lista de palabras obtenidas.
    * `arcade`. Borra la partida guardada de modo arcade y su mapa de bombas o bonos activados; no borra los puntajes más altos o la lista de palabras obtenidas.
    * `data`. Borra lo guardado en `foo.data`.

> Ojo: los resultados de esta función son irreversibles.

## `localStorage`

Varios datos del juego se guardan en [`localStorage`](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage).

Se guardan según el idioma, con la coletilla `_lang` donde `lang` es igual
al idioma en juego, como `_es`.

* `story_es || arcade_es`. `Object` que guarda el estado de la partida.
* `story_bombs_bonus_es || arcade_bombs_bonus_es`. `Array` que guarda un mapa de las bombas o bonos ya activados.
* `story_words_es || arcade_words_es`. `Object` que guarda las palabras encontradas, su cantidad y la cantidad de todas las palabras de una modalidad.
* `story_highest_scores_es || arcade_highest_scores_es`. `Array` que guarda los mayores puntajes de cada modalidad.
