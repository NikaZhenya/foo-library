# `create_jsons.rb` 

Este _script_ convierte de TXT a JSON. Está pensado para simplificar
la creación de las estructuras necesarias para esta librería.

## Uso

```
ruby create_jsons.rb
```

El _script_ requiere de:

* [Ruby](https://www.ruby-lang.org/).
* Una historia en `txt/story.txt`.
* Una lista de palabras en `txt/arcade.txt`.

Esto creará una estructura básica. Para mayor jugabilidad, es 
recomendable cambiar propiedades de manera manual.
